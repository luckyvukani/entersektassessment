﻿using AventStack.ExtentReports;
using EntersektAssessment.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntersektAssessment.Tests
{
    [TestFixture("http://localhost:8081")]
    public class ToDoListTest : TestBase
    {
        string url;
        string item = "Test Item";
        ElementHelper toDo = new ElementHelper(/*driver*/);
        static ExtentTest Report = Reports.CreateTest("Items Report");
        ExtentTest Reportnode = Report.CreateNode("Items Node");

        public ToDoListTest(string url)
        {
            this.url = url;
        }

        [Test]
        public void AddItem()
        {
            driver.Navigate().GoToUrl(url);
            //Add Items
            toDo.AddItem(Reportnode, item);
            //Check if Todo list items  persist after browser refresh
            driver.Navigate().Refresh();
            if (IsElementDisplay.IsElementDisplayed(By.XPath("//span[contains(@id,'span-todo')][text()='" + item + "']"), driver))
            {
                Console.WriteLine("Item Persist after Refresh");
            }

        }

        [Test]
        public void UpdateItem()
        {
            driver.Navigate().GoToUrl(url);
            toDo.AddItem(Reportnode, item);
            toDo.UpdateItem(Reportnode, driver, item);
        }

        [Test]
        public void DeleteItem()
        {
            driver.Navigate().GoToUrl(url);
            toDo.AddItem(Reportnode, item);
            toDo.DeleteItem(Reportnode, driver, item);
            if (!IsElementDisplay.IsElementDisplayed(By.XPath("//span[contains(@id,'span-todo')][text()='" + item + "']"), driver))
            {
                Console.WriteLine("Item Deleted");
            }

        }
    }
}
