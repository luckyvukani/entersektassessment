﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntersektAssessment
{
    public class IsElementDisplay
    {
        public static bool IsElementDisplayed(By ElementLocator, IWebDriver driver)
        {
            //var Driver = driver;
            try
            {
                var result = driver.FindElement(ElementLocator).Displayed;
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
