﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntersektAssessment.POM
{
    public class Elements
    {
        
        public  IWebElement Additems => TestBase.driver.FindElement(By.XPath("//input[@name='newtodo']"));
        public  IWebElement BtnAdd => TestBase.driver.FindElement(By.XPath("//input[@id='new-submit']"));
        public  IWebElement UpdateTextbox => TestBase.driver.FindElement(By.XPath("//input[@name='edittodo']"));
        public  IWebElement UpdateBtn => TestBase.driver.FindElement(By.XPath("//input[@id='edit-submit-0']"));
        public  IWebElement DeleteBtn => TestBase.driver.FindElement(By.XPath("//form[@class='edit-todo-form']/a[@href='/todo/delete/0']"));
    }
}
