﻿using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EntersektAssessment.POM
{
    public class ElementHelper : Elements
    {
        public void AddItem(ExtentTest test, string item)
        {
            Additems.SendKeys(item);
            test.Log(Status.Pass,"Item Added Successful");
            BtnAdd.Click();
            test.Log(Status.Pass, "Add Button Clicked");
        }

        public void UpdateItem(ExtentTest test,IWebDriver driver, string item)
        {
            UpdateTextbox.SendKeys(item);
            test.Log(Status.Pass, "Item entered in the textbox");
            UpdateBtn.Click();
            test.Log(Status.Pass, "Update Button Clicked");
        }

        public void DeleteItem(ExtentTest test, IWebDriver driver, string item)
        {
            Thread.Sleep(2000);
            DeleteBtn.Click();
            test.Log(Status.Pass, "Delete Button Clecked");
        }
    }
}
