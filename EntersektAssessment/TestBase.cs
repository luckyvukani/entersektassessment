﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntersektAssessment
{
    using ExtentReports = AventStack.ExtentReports.ExtentReports;
    public class TestBase
    {
        public ExtentV3HtmlReporter ExtentReports;
        public static ExtentReports Reports = new ExtentReports();
        public static ExtentTest test;
        public static IWebDriver driver;
        static string filePath = AppDomain.CurrentDomain.BaseDirectory;
        static int bin = filePath.LastIndexOf("bin");
        public string projectDirectory = filePath.Remove(bin);

        [SetUp]
        public void BeforeTest()
        {
            string pth = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            string ActualPath = pth.Substring(0, pth.LastIndexOf("bin"));
            string projectPath = new Uri(ActualPath).LocalPath;

            string reportPath = projectPath + "Reports\\TestRunReport.html";
            Reports.AddTestRunnerLogs(projectPath + "Extent-COnfig.html");
            ExtentReports = new ExtentV3HtmlReporter(reportPath);
            Reports.AttachReporter(ExtentReports);
            driver = new ChromeDriver(projectDirectory + "\\DriverManager");
        }
        [TearDown]
        public void AfterTest()
        {
            driver.Close();
            driver.Quit();
            Reports.Flush();
        }
    }
}
